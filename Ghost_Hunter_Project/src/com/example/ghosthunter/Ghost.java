package com.example.ghosthunter;

import android.graphics.Bitmap;

public class Ghost extends Droid {

    Speed speed;
    
    public Ghost(Bitmap bitmap, int x, int y, float xv, float yv) {
        
        super(bitmap,x,y);
        speed = new Speed(xv,yv);
    }
    
    public Speed getSpeed()
    {
        return this.speed;
    }

}