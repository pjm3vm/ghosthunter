package com.example.ghosthunter;

import com.example.ghosthunter.R;
import android.app.Service;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.content.ServiceConnection;
import android.view.View;
import android.widget.Toast;

import android.os.Bundle;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.view.Menu;
import android.view.View;
import android.os.IBinder;
import android.widget.Button;

public class MainActivity extends Activity implements ServiceConnection{
	private boolean mIsBound = false;
	private MusicService mServ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		doBindService();
		Intent music = new Intent();
		music.setClass(this,MusicService.class);
		startService(music);
		
	}
	
	public void launchSecondaryActivity(View view) {
        Intent intent = new Intent(this, OptionsMenu.class);
        startActivity(intent);
        
    }
	
	public void launchGhostHunter(View view){
		Intent intent = new Intent(this, DroidzActivity.class);
        startActivity(intent);
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder binder) {
		// TODO Auto-generated method stub
		mServ = ((MusicService.ServiceBinder) binder).getService();
		
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		// TODO Auto-generated method stub
		mServ = null;
	}
	
	public void doBindService()
	{
		// activity connects to the service.
 		Intent intent = new Intent(this, MusicService.class);
		bindService(intent, this, Context.BIND_AUTO_CREATE);
		mIsBound = true;
	}
	
	public void doUnbindService()
	{
		// disconnects the service activity.
		if(mIsBound)
		{
			unbindService(this);
      		mIsBound = false;
		}
	}
	// when closing the current activity, the service will automatically shut down(disconnected).
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		
		doUnbindService();
	}
	
}
