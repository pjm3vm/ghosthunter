package com.example.ghosthunter;

import com.example.ghosthunter.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ImageView;

public class GhostHunter extends Activity{
	private ImageView ghost_hunter;
	private ImageView blue_wall_top_border;
	private ImageView blue_wall_bottom_border;
	private ImageView blue_wall_left_border;
	private ImageView blue_wall_right_border;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); //Do all the setup associated with the Activity class
        setContentView(R.layout.activity_ghost_hunter); //Set our view to activity_pretty.xml
        connectLayoutItems(); //Connect to the TextView and Button
        
    }

    /*
    Connect to the Button and TextView in the Layout (activity_hello.xml)
     */
    public void connectLayoutItems() {
        ghost_hunter = (ImageView) findViewById(R.id.ghost_hunter);
        blue_wall_top_border = (ImageView) findViewById(R.id.blue_wall_top_border);
        blue_wall_bottom_border = (ImageView) findViewById(R.id.blue_wall_bottom_border);
        blue_wall_left_border = (ImageView) findViewById(R.id.blue_wall_left_border);
        blue_wall_right_border = (ImageView) findViewById(R.id.blue_wall_right_border);
    }
}
