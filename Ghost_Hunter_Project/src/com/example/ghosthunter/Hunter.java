package com.example.ghosthunter;

import android.graphics.Bitmap;

public class Hunter extends Droid {

    private Bitmap bitmap;    // the actual bitmap
    private int x;            // the X coordinate
    private int y;            // the Y coordinate
    private boolean touched;    // if droid is touched/picked up
    
    public Hunter(Bitmap bitmap, int x, int y)
    {
        super(bitmap, x, y);
    }
    
    public boolean isTouched() {
        return touched;
    }

    public void setTouched(boolean touched) {
        this.touched = touched;
    }
    
    public void handleActionDown(int eventX, int eventY) {
        if (eventX >= (x - bitmap.getWidth() / 2) && (eventX <= (x + bitmap.getWidth()/2))) {
            if (eventY >= (y - bitmap.getHeight() / 2) && (y <= (y + bitmap.getHeight() / 2))) {
                // droid touched
                setTouched(true);
            } else {
                setTouched(false);
            }
        } else {
            setTouched(false);
        }
    }
    
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

}