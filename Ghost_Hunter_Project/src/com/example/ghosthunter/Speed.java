package com.example.ghosthunter;

public class Speed {

	public static final int Right = 1;

 	public static final int Left = -1;

	public static final int Up = -1;

	public static final int Down = 1;

	private int Xdirection = Right;

	private int Ydirection = Down;

	private float Xvelocity;

	private float Yvelocity;

	public Speed() {

		this.Xvelocity = 1;

		this.Yvelocity = 1;

	}

	public Speed(float Xvelocity, float Yvelocity) {

		this.Xvelocity = Xvelocity;

		this.Yvelocity = Yvelocity;

	}

	public int getXdirection() {

		return Xdirection;

	}

	public void setXdirection(int xdirection) {

		Xdirection = xdirection;

	}

	public int getYdirection() {

		return Ydirection;

	}

	public void setYdirection(int ydirection) {

		Ydirection = ydirection;

	}

	public float getXvelocity() {

		return Xvelocity;

	}

	public void setXvelocity(float xvelocity) {

		Xvelocity = xvelocity;

	}

	public float getYvelocity() {

		return Yvelocity;

	}

	public void setYvelocity(float yvelocity) {

		Yvelocity = yvelocity;

	}

	public void changeXDirection() {

		Xdirection = Xdirection * -1;

	}

	public void changeYDirection() {

		Ydirection = Ydirection * -1;

	}

}