package com.example.ghosthunter;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MainGamePanel extends SurfaceView implements
		SurfaceHolder.Callback {

	private static final String TAG = MainGamePanel.class.getSimpleName();
	private MainThread thread;
	private Droid droid;
	private int droidX;
	private int droidY;
	private Hunter ghostHunter;
	private Ghost ghost1;

	public MainGamePanel(Context context) {
		super(context);
		getHolder().addCallback(this);

		ghostHunter = new Hunter(BitmapFactory.decodeResource(getResources(),
				R.drawable.ghost_hunter), 50, 50);
		//ghost1 = new Ghost()
		// create the game loop thread
		thread = new MainThread(getHolder(), this);

		setFocusable(true);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		thread.setRunning(true);
		thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		while (retry) {
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
				// try again shutting down the thread
			}
		}
	}

	@Override
	 public boolean onTouchEvent(MotionEvent event) {
	  if (event.getAction() == MotionEvent.ACTION_DOWN) {
	   // delegating event handling to the droid
	   droid.handleActionDown((int)event.getX(), (int)event.getY());

	   // check if in the lower part of the screen we exit
	   if (event.getY() > getHeight() - 50) {
	    thread.setRunning(false);
	    ((Activity)getContext()).finish();
	   } else {
	    Log.d(TAG, "Coords: x=" + event.getX() + ",y=" + event.getY());
	   }
	  } if (event.getAction() == MotionEvent.ACTION_MOVE) {
	   // the gestures
	   if (droid.isTouched()) {
	    // the droid was picked up and is being dragged
	    droid.setX((int)event.getX());
	    droid.setY((int)event.getY());
	   }
	  } if (event.getAction() == MotionEvent.ACTION_UP) {
	   // touch was released
	   if (droid.isTouched()) {
	    droid.setTouched(false);
	   }
	  }
	  return true;
	 }
	
	public void update() {
		// check collision with right wall if heading right
		if (droid.getSpeed().getXdirection() == Speed.Right
				&& droid.getX() + droid.getBitmap().getWidth() / 2 >= getWidth()) {
			droid.getSpeed().changeXDirection();
		}
		// check collision with left wall if heading left
		if (droid.getSpeed().getXdirection() == Speed.Left
				&& droid.getX() - droid.getBitmap().getWidth() / 2 <= 0) {
			droid.getSpeed().changeXDirection();
		}
		// check collision with bottom wall if heading down
		if (droid.getSpeed().getYdirection() == Speed.Down
				&& droid.getY() + droid.getBitmap().getHeight() / 2 >= getHeight()) {
			droid.getSpeed().changeYDirection();
		}
		// check collision with top wall if heading up
		if (droid.getSpeed().getYdirection() == Speed.Up
				&& droid.getY() - droid.getBitmap().getHeight() / 2 <= 0) {
			droid.getSpeed().changeYDirection();
		}
		// Update the lone droid
		droid.update();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(Color.BLACK);
		droid.draw(canvas);
	}

}
