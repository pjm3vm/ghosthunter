package com.example.ghosthunter;

import com.example.ghosthunter.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.Button;
import android.widget.TextView;

public class OptionsMenu extends Activity{
	//private TextView hello_world_text;
	ImageButton easyDifficultyButton;
	ImageButton mediumDifficultyButton;
	ImageButton hardDifficultyButton;
	 
	 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); //Do all the setup associated with the Activity class
        setContentView(R.layout.activity_options_menu); //Set our view to activity_pretty.xml
                
    }

    public void launchAdjustDifficulty(View view){
		Intent intent = new Intent(this, GhostHunter.class);
        startActivity(intent);
		
		
	}
    
    public void addListenerOnButton() {
    	 
		easyDifficultyButton = (ImageButton) findViewById(R.id.easyDifficultyButton);
		mediumDifficultyButton = (ImageButton) findViewById(R.id.mediumDifficultyButton);
		hardDifficultyButton = (ImageButton) findViewById(R.id.hardDifficultyButton);
		
		easyDifficultyButton.setOnClickListener(new OnClickListener() {
 
			@Override
			public void onClick(View arg0) {
 
			   Toast.makeText(OptionsMenu.this,
				"Easy Difficulty Selected is clicked!", Toast.LENGTH_LONG).show();
 
			}
 
		});
		mediumDifficultyButton.setOnClickListener(new OnClickListener() {
			 
			@Override
			public void onClick(View arg0) {
 
			   Toast.makeText(OptionsMenu.this,
				"Medium Difficulty Selected is clicked!", Toast.LENGTH_LONG).show();
 
			}
 
		});
		hardDifficultyButton.setOnClickListener(new OnClickListener() {
			 
			@Override
			public void onClick(View arg0) {
 
			   Toast.makeText(OptionsMenu.this,
				"Hard Difficulty Selected is clicked!", Toast.LENGTH_LONG).show();
 
			}
 
		});
 
	}
    	
}
